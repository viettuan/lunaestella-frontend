// client side config
const appConfig = process.env.APP_CONFIG;
appConfig.env = process.env.ENV;
const { api } = appConfig;

appConfig[appConfig.env] = true;

if (appConfig.env !== 'production') {
  console.log('- ENV::', process.env.ENV);
  console.log('- NODE_ENV::', process.env.NODE_ENV);
}

api.endPoint = global.isClient ? appConfig.api.browserEndPoint : api.nodeEndPoint;

export default appConfig;
