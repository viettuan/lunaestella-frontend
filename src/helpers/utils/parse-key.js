/* eslint-disable */

function snakeToCamel(str) {
  return str.replace(/_{1,}([a-z0-9A-Z])/g, (regex, replacer) => {
    return replacer.toUpperCase();
  });
}

function camelToSnake(str) {
  return str.replace(/([a-z0-9])([A-Z])/g, (regex, s1, s2) => {
    return `${s1}_${s2.toLowerCase()}`;
  });
}

export function parseSnakeToCamel(data) {
  if (data) {
    if (Array.isArray(data)) {
      return data.map(e => parseSnakeToCamel(e));
    } else if (typeof data === 'object') {
      const res = {};
      Object.entries(data).forEach(([key, value]) => {
        const camelKey = snakeToCamel(key);
        res[camelKey] = parseSnakeToCamel(value);
      });
      return res;
    }
  }
  return data;
}

export function parseCamelToSnake(data) {
  if (data) {
    if (Array.isArray(data)) {
      return data.map(e => parseCamelToSnake(e));
    } else if (typeof data === 'object') {
      const res = {};
      Object.entries(data).forEach(([key, value]) => {
        const snakeKey = camelToSnake(key);
        res[snakeKey] = parseCamelToSnake(value);
      });
      return res;
    }
  }
  return data;
}
