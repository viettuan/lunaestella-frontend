import config from '@/config';

const dev = {
  log(...args) {
    if (config.development) {
      console.log.apply(null, args);
    }
  },
  error(...args) {
    if (config.development) {
      console.error.apply(null, args);
    }
  },
  warn(...args) {
    if (config.development) {
      console.warn.apply(null, args);
    }
  },
  run(func) {
    if (config.development) {
      func();
    }
  },
};

export default dev;
