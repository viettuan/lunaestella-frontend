import Cookies from 'universal-cookie';

export const cookies = new Cookies({ path: '/' });
const tokenKey = 'access_token';
const userKey = 'user';

export const CookieTerm = {
  key: 'accepted_cookies',
  isAccepted() {
    return cookies.get(CookieTerm.key);
  },
  accept() {
    return cookies.set(CookieTerm.key, true, {
      maxAge: 20 * 365 * 24 * 3600, // 20 years
    });
  },
};

class ClientStorageHelper {
  static getToken() {
    return cookies.get(tokenKey);
  }

  static clearUserData() {
    ClientStorageHelper.clearToken();
    cookies.remove(userKey);
  }

  static saveTokenData({ tokenType, accessToken }) {
    cookies.set(tokenKey, `${tokenType} ${accessToken}`);
  }

  static clearToken() {
    cookies.remove(tokenKey);
  }
  static clearAllData() {
    cookies.remove(tokenKey);
  }
}

export default ClientStorageHelper;
