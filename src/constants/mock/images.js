import { firstImages } from './product-list';

const mockImages = {
  slider: firstImages,
};

export default mockImages;
