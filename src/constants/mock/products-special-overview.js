const sampleDefault = {
  category: '',
  price: '52 CHF',
  unit: '',
  rating: {
    value: 3,
    total: 16,
  },
  promotion: false,
  gift: false,
  subTitle: 'Eventtype',
  title: 'Special XY',
  speicalType: 'Specialtype',
  description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam tempor invidunt.',
};

export const firstImages = [
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
];

const random = [
  {
    promotion: 'New',
    numberLocation: 'at 13 places',
  },
  {
    promotion: false,
    numberLocation: 'at 13 places',
  },
  {
    promotion: false,
    numberLocation: false,
  },
  {
    promotion: 'New',
    numberLocation: 'at 13 places',
  },
  {
    promotion: false,
    numberLocation: false,
  },
  {
    promotion: false,
    numberLocation: false,
  },
  {
    promotion: false,
    numberLocation: 'at 13 places',
  },
  {
    promotion: false,
    numberLocation: false,
  },
];

const productList = random.map((p, i) => {
  return {
    ...sampleDefault,
    images: [
      firstImages[i],
      ...firstImages,
    ],
    ...p,
  };
});

export default productList;

