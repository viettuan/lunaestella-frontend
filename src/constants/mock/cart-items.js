// demo data
export const gift = {
  type: 'gift',
  typeAlias: 'product',
  typeName: 'Your Gift',
  name: 'Gift Product XY',
  price: {
    value: '52 CHF',
    unit: 'Price',
  },
  options: [
    {
      name: 'Size',
      value: 'Medium',
    },
    {
      name: 'Color',
      isColor: true,
      value: '#00A433',
      colorName: 'green',
    },
  ],
};

export const gift2 = {
  ...gift,
  options: [
    {
      name: 'Size',
      value: 'Medium',
    },
    {
      name: 'Color',
      isColor: true,
      value: '#DB7676',
      colorName: 'red',
    },
    {
      name: 'Amount',
      value: 1,
    },
    {
      name: 'Delivery',
      value: 'at home',
    },
    {
      name: 'Arrival date',
      value: '04.10.2018',
    },
  ],
};


export const gift3 = {
  ...gift,
  options: [
    {
      name: 'Color',
      isColor: true,
      value: '#DB7676',
      colorName: 'red #1',
    },
    {
      name: 'Size',
      value: 'M',
    },
    {
      name: 'Amount',
      value: 1,
    },
    {
      name: 'Shipping',
      value: '9.50 CHF',
    },
  ],
};

export const location = {
  type: 'location',
  typeName: 'Your Location',
  name: 'High class Resort Hotel Bellevue',
  thumbnail: require('@/assets/uploads/location-thumb.png'),
  price: {
    value: '522 CHF',
    unit: 'Price for two persons',
  },
  options: [
    {
      name: 'Arrival date',
      value: '02.10.2018',
    },
    {
      name: 'Nights',
      value: '2',
    },
    {
      name: 'Departure date',
      value: '04.10.2018',
    },
    {
      name: 'Your hotel add-ons',
      value: 'Basic',
    },
    {
      name: 'Your room type',
      value: 'Standard room',
    },
  ],
};

export const location2 = {
  ...location,
  options: [
    {
      name: 'Arrival date',
      value: '02.10.2018',
    },
    {
      name: 'Nights',
      value: '2',
    },
  ],
};

export const special = {
  type: 'special',
  typeName: 'Your Special',
  name: 'Special XY',
  price: {
    value: '522 CHF',
    unit: 'Price for two persons',
  },
  options: [
    {
      name: 'Eventtype',
      value: 'Event 1',
    },
    {
      name: 'Eventlocation',
      value: 'Location 1',
    },
    {
      name: 'People',
      value: '2',
    },
    {
      name: 'Event date',
      value: '04.10.2018',
    },
  ],
};

export const special2 = {
  ...special,
  options: [
    {
      name: 'Date',
      value: '03.10.2018',
    },
    {
      name: 'Eventlocation',
      value: 'Location 1',
    },
  ],
};

/* eslint-disable */
export const cartPackage = {
  name: '[name]',
  content: {
    title: 'Generated Package text.',
    text:
      'Package Description generated. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam.',
  },
  products: [gift, location, special],
};
