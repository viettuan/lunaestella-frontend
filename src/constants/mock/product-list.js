import icons from '../../common/icons';

const sampleDefault = {
  type: 'location',
  price: '522 CHF',
  unit: 'per night for two person',
  rating: {
    value: 3,
    total: 16,
  },
  promotion: false,
  gift: false,
  addOn: [
    {
      title: 'Basic',
      subTitle: 'Romantic stay for lovers',
      checked: true,
      breakfast: 'Breakfast included',
      aperitif: '1x aperitif',
      cuddle: 'Cuddle until the early afternoon (until 14 o\'clock)',
      thingList: [],
    },
    {
      title: 'Premium',
      subTitle: 'Overnight stay with romantic foam bath',
      checked: false,
      breakfast: 'Breakfast included',
      aperitif: '1x aperitif',
      cuddle: 'Cuddle until the early afternoon (until 14 o\'clock)',
      thingList: [
        {
          icon: icons.romantic,
          text: 'Romantic room decoration',
        },
        {
          icon: icons.rose,
          text: 'Rose petals in the free-standing bath tub',
        },
        {
          icon: icons.bath,
          text: 'Enjoy a romantic bubble bath in your boutique room',
        },
        {
          icon: icons.kitchen,
          text: 'Romantic 5-course candlelight dinner on arrival day (excl. drinks)',
        },
      ],
    },
    {
      title: 'Luxury',
      subTitle: 'Romantic days of enjoyment in Lucerne',
      checked: false,
      breakfast: 'Breakfast included',
      aperitif: '1x aperitif',
      cuddle: 'Cuddle until the early afternoon (until 14 o\'clock)',
      thingList: [
        {
          icon: icons.kitchen,
          text: 'One 3-course dinner and apéro including appetizers in the spice shop (1st evening, exkl. drinks)',
        },
        {
          icon: icons.kitchen,
          text: 'One wine accompaniment to 3 courses (1st evening)',
        },
        {
          icon: icons.kitchen,
          text: 'One 5-course candlelight dinner (2nd evening, excl. drinks)',
        },
        {
          icon: icons.roses,
          text: 'A bouquet of roses in your room',
        },
        {
          icon: icons.wedding,
          text: 'One bottle of Rosé Prosecco in your room\'s ice cooler',
        },
        {
          icon: icons.nature,
          text: 'One romantic hike including a round trip ticket cable car – hut Z – bus back to the hotel',
        },
      ],
    },
  ],
};

export const firstImages = [
  require('@/assets/uploads/product-md-1.png'),
  require('@/assets/uploads/product-md-2.png'),
  require('@/assets/uploads/product-md-3.png'),
  require('@/assets/uploads/product-md-4.png'),
  require('@/assets/uploads/product-md-5.png'),
  require('@/assets/uploads/product-md-6.png'),
  require('@/assets/uploads/product-md-7.png'),
  require('@/assets/uploads/product-md-8.png'),
];

const random = [
  {
    subTitle: 'Lucerne, Switzerland',
    title: 'Romantic birthday dinner for two in a historical hotel train',
    services: ['luxury', 'kitchen'],
  },
  {
    subTitle: 'Berne, Switzerland',
    title: 'Romantic birthday in a tree house in the forest',
    services: ['bath', 'nature'],
  },
  {
    subTitle: 'St Moritz, Switzerland',
    title: 'Hotel Alpenrose alpine spa',
    services: ['luxury', 'kitchen'],
  },
  {
    subTitle: 'St. Antonien, Switzerland',
    title: 'Cuddling at alpine resort with snowy adventure',
    promotion: '20% OFF in November',
    services: ['bath', 'nature'],
  },
  {
    subTitle: 'Lucerne, Switzerland',
    title: 'High class Resort Hotel Bellevue',
    services: ['luxury', 'kitchen', 'bath'],
  },
  {
    subTitle: 'Lucerne, Switzerland',
    title: 'Mountain hotel for lovers with a special adventure',
    services: ['nature'],
  },
  {
    subTitle: 'Lucerne, Switzerland',
    title: 'Luxus Hotel Löwen with a cosy restaurant by the lake',
    services: ['luxury', 'kitchen', 'bath'],
  },
  {
    subTitle: 'St Moritz, Switzerland',
    title: 'Romantic Hotel in St. Moritz',
    services: ['luxury', 'kitchen', 'bath'],
  },
];

const productList = random.map((p, i) => {
  return {
    ...sampleDefault,
    images: [
      firstImages[i],
      ...firstImages,
    ],
    ...p,
  };
});

export default productList;
