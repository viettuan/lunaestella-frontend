const sampleDefault = {
  price: '522 CHF',
  unit: '',
  promotion: false,
  gift: false,
  description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam tempor invidunt.',
  attribute: [
    {
      name: 'size',
      value: ['xs', 's', 'm', 'l', 'xl'],
    },
    {
      name: 'color',
      value: ['#262124', '#4A5A8D', '#00A433', '#8D6D16'],
    },
  ],
};

export const firstImages = [
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
  require('@/assets/images/1px.png'),
];

const random = [
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [
      '#00A433', '#8E5885', '#BD2519',
    ],
    promotion: 'New',
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [
      '#262124', '#00A433', '#8E5885',
    ],
    promotion: false,
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [],
    promotion: false,
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [
      '#00A433', '#5F7142',
    ],
    promotion: false,
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [],
    promotion: false,
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [
      '#262124', '#4A5A8D', '#00A433', '#8D6D16',
    ],
    promotion: false,
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [
      '#BD2519',
    ],
    promotion: false,
  },
  {
    title: 'Gift Product XY',
    subTitle: 'Producttype',
    colors: [
      '#262124', '#00A433',
    ],
    promotion: 'New',
  },
  {
    title: 'Product XY',
    subTitle: 'Subcategory',
    colors: [
      '#262124',
    ],
    promotion: false,
  },
];

const productList = random.map((p, i) => {
  return {
    ...sampleDefault,
    images: [
      firstImages[i],
      ...firstImages,
    ],
    ...p,
  };
});

export default productList;

