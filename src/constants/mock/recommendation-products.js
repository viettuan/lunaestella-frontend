import mockImages from './images';

const list = [
  {
    images: [require('@/assets/uploads/pro1.png'), mockImages.slider[0]],
    title: 'Gift Product XY',
    location: 'Producttype',
    price: '522 CHF',
    unit: 'per night for two person',
    listSpecials: ['Lorem ipsum about the gift. ', 'The safari suite in a tree house hotel.', 'Dinner «Tarzan & Jane» among tree tops.'],
    rating: {
      value: 3,
      total: 16,
    },
    promotion: false,
    gift: true,
    services: ['luxury', 'bath'],
  },
  {
    images: [require('@/assets/uploads/pro2.png'), mockImages.slider[1]],
    title: 'Gift Product XY',
    location: 'Producttype',
    price: '522 CHF',
    unit: 'per night for two person',
    listSpecials: ['Lorem ipsum about the gift. ', 'The safari suite in a tree house hotel.', 'Dinner «Tarzan & Jane» among tree tops.'],
    rating: {
      value: 3,
      total: 16,
    },
    promotion: false,
    gift: true,
    services: ['luxury', 'kitchen'],
  },
];

const mockProducts = {
  list,
};

const makeSpecial = e => ({
  ...e,
  title: 'Special XY',
  subTitle: 'Specialtype',
  type: 'special',
  price: '52 CHF',
  unit: null,
});

const makeGift = e => ({
  ...e,
  title: 'Gift XY',
  subTitle: 'Subcategory',
  type: 'gift',
  price: '52 CHF',
  unit: null,
});

const makeLocation = e => ({
  ...e,
  title: 'Location XY',
  subTitle: 'Locationtype',
  type: 'location',
  price: '52 CHF',
  unit: null,
});

export const special = makeSpecial(list[0]);
export const location = makeLocation(list[0]);
export const gift = makeGift(list[0]);

export const recommendGifts = list.map(makeGift);

export const recommendSpecials = list.map(makeSpecial);

export const recommendLocations = list.map(makeLocation);

export const recommendLocationSpecials = list.map((e, i) => (i % 2 ? makeSpecial(e) : makeLocation(e)));

export const recommendGiftLocations = list.map((e, i) => (i % 2 ? makeGift(e) : makeLocation(e)));

export default mockProducts;
