import mockImages from './images';

const mockProducts = {
  type: 'location',
  single: {
    images: [
      require('@/assets/uploads/product1.png'),
      mockImages.slider,
    ],
    title: 'Romantic birthday dinner for two in a historical train',
    subTitle: 'Lucerne, Switzerland',
    price: '522 CHF',
    unit: 'per night for two person',
    listSpecials: [
      'Lorem ipsum about the gift. Lorem ipsum valorat about the gift. Lorem ipsum',
      'The safari suite in a tree house hotel. The safari suite in a tree house hotel.',
      'Dinner «Tarzan & Jane» among tree tops.',
    ],
    rating: {
      value: 4,
      total: 16,
    },
    promotion: '20% OFF in November',
    gift: true,
  },
  list: [
    {
      type: 'location',
      images: [
        require('@/assets/uploads/product1.png'),
        mockImages.slider[0],
      ],
      title: 'Romantic birthday dinner for two in a historical train',
      subTitle: 'Lucerne, Switzerland',
      price: '522 CHF',
      unit: 'per night for two person',
      listSpecials: [
        'Lorem ipsum about the gift. ',
        'The safari suite in a tree house hotel.',
        'Dinner «Tarzan & Jane» among tree tops.',
      ],
      rating: {
        value: 3,
        total: 16,
      },
      promotion: false,
      gift: true,
    },
    {
      type: 'location',
      images: [
        require('@/assets/uploads/product2.png'),
        mockImages.slider[1],
      ],
      title: 'Cuddling at alpine resort with snowy adventure',
      subTitle: 'St. Antonien, Switzerland',
      price: '522 CHF',
      unit: 'per night for two person',
      listSpecials: [
        'Lorem ipsum about the gift. ',
        'The safari suite in a tree house hotel.',
        'Dinner «Tarzan & Jane» among tree tops.',
      ],
      rating: {
        value: 3,
        total: 16,
      },
      promotion: false,
      gift: true,
    },
  ],
};

export default mockProducts;