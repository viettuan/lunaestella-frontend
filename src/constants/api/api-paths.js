import appConfig from '@/config';
import { objectMap } from '@/helpers/utils';

const {
  api: { endPoint },
} = appConfig;

// REST
const apiPaths = {
  general: {
    config: '/config',
  },
  users: {
    login: '/users/login',
  },
  products: {
    base: '/products',
    single: '/products/:id',
  },
  blogs: {
    base: '/blogs',
    single: '/blogs/:id',
  },
  menus: {
    base: '/menus',
    single: '/menus/:id',
  },
  services: {
    base: '/services',
    single: '/services/:id',
  },
  tours: {
    base: '/tours',
    single: '/tours/:id',
  },
};

objectMap(apiPaths, e =>
  objectMap(e, (path, key) => {
    e[key] = endPoint + e[key];
  }),
);

export default apiPaths;
