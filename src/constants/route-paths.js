import locationHelper from '@/helpers/location';

const routePaths = {
  mainApp: '/',
  staticHtml: '/static-html',
  packages: {
    overview: '/packages/overview',
    detail: '/packages/:id',
  },
  gifts: {
    overview: '/gifts/overview',
    detail: '/gifts/:id',
  },
  locations: {
    overview: '/locations/overview',
    detail: '/locations/:id',
  },
  specials: {
    overview: '/specials/overview',
    valentineDay: '/specials/valentine',
  },
};

routePaths.getLink = function (path, params) {
  locationHelper.getLink(path, params);
};

export default routePaths;
