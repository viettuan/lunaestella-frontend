import { formatDate as formatHelper } from '@/helpers/utils/datetime';

export const DATE_FORMAT = 'dd.mm.yyyy';
export const formatDate = value => formatHelper(value, DATE_FORMAT);
export const ALL_LOCALES = [
  {
    value: 'en',
    label: 'English',
    fullLabel: 'ENGLISH',
  },
  {
    value: 'fr',
    label: 'Francais',
    fullLabel: 'FRANÇAIS',
  },
  {
    value: 'de',
    label: 'Deutsch',
    fullLabel: 'Deutsch',
  },
];

export const ALL_COUNTRIES = [
  {
    value: 'c-sw',
    label: 'Switzerland',
    flag: require('@/assets/icons/flag-ch.svg'),
  },
  {
    value: 'c-fr',
    label: 'France',
    flag: require('@/assets/icons/flag-eu.svg'),
  },
  {
    value: 'c-ge',
    label: 'Germany',
    flag: require('@/assets/icons/flag-de.svg'),
  },
];
