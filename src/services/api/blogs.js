import apiPaths from '@/constants/api/api-paths';
import RestApiAbstract from './rest-api-abstract';

export class BlogsApiService extends RestApiAbstract {
  resource = apiPaths.blogs;

  parseResponse = (data) => {
    return {
      title: data.title,
      slug: data.slug,
      link: `/blogs/${data.slug}.html`, // generate link
      featured: !!data.featured,
      thumbnail: data.thumbnail,
      doubleImages: data.double_images,
      excerpt: data.excerpt,
    };
  };
}