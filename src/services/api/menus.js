import apiPaths from '@/constants/api/api-paths';
import locationHelper from '@/helpers/location';
import routes from '@/router/routes';
import RestApiAbstract from './rest-api-abstract';

class MenusApiService extends RestApiAbstract {
  resource = apiPaths.menus;

  parseResponse = data => ({
    ...data,
    permalink: '/cates',
  });
}

export default MenusApiService;
