import apiPaths from '@/constants/api/api-paths';
import RestApiAbstract from './rest-api-abstract';

class ServicesApiService extends RestApiAbstract {
  resource = apiPaths.services;
}

export default ServicesApiService;
