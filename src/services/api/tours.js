import apiPaths from '@/constants/api/api-paths';
import locationHelper from '@/helpers/location';
import routes from '@/router/routes';
import RestApiAbstract from './rest-api-abstract';

class ToursApiService extends RestApiAbstract {
  resource = apiPaths.tours;

  parseResponse = data => ({
    ...data,
    images: new Array(2).fill(data.avatar),
    permalink: locationHelper.getLink(routes.tours.detail.path, data),
  });
}

export default ToursApiService;
