import apiPaths from '@/constants/api/api-paths';
import RestApiAbstract from './rest-api-abstract';

class ProductsApiService extends RestApiAbstract {
  resource = apiPaths.products;
}

export default ProductsApiService;
