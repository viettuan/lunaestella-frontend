import { createRoute } from '@/helpers/vue/route-helper';
import CartPage from '@/modules/cart-checkout/views/CartPage';
import CheckoutPage from '@/modules/cart-checkout/views/CheckoutPage';
import OrderConfirmation from '@/modules/cart-checkout/views/OrderConfirmation';
import routePaths from './route-paths';
// layouts
import ProfileLayout from '../layouts/ProfileLayout';
import DefaultLayout from '../layouts/DefaultLayout';
import BlankLayout from '../layouts/BlankLayout';

// components
import InstaModal from '../components/InstaModal';
// pages
import Home from '../views/Home';
import LocationOverview from '../views/LocationOverview';
import StyleGuide from '../views/StyleGuide';
import Sitemap from '../views/Sitemap';
import About from '../views/About';
import SpecialDay from '../views/SpecialDay';
import Packages from '../views/Packages';
import SpecialGifts from '../views/SpecialGifts';
import GiftDetail from '../views/GiftDetail';
import SpecialOverview from '../views/SpecialOverview';
import SpecialDetail from '../views/SpecialDetail';
import LocationDetail from '../views/LocationDetail';
import MyAccount from '../views/MyAccount';
import SimpleContent from '../views/SimpleContent';


const routes = {
  index: createRoute({
    path: routePaths.sitemap,
    name: 'static-index',
    component: Sitemap,
    meta: {
      layout: BlankLayout,
    },
  }),
  home: createRoute({
    path: routePaths.home,
    name: 'static-home',
    component: Home,
    meta: {
      layout: DefaultLayout,
    },
  }),
  styleGuide: createRoute({
    path: routePaths.styleGuide,
    name: 'style-guide',
    component: StyleGuide,
    meta: {
      layout: BlankLayout,
    },
  }),
  locationOverview: createRoute({
    path: routePaths.locationOverview,
    name: 'location-overview',
    component: LocationOverview,
    meta: {
      layout: DefaultLayout,
    },
  }),
  about: createRoute({
    path: routePaths.about,
    name: 'static-about',
    component: About,
    meta: {
      layout: DefaultLayout,
    },
  }),
  speicalDay: createRoute({
    path: routePaths.valentineSpecial,
    name: 'static-special-day',
    component: SpecialDay,
    meta: {
      layout: DefaultLayout,
    },
  }),
  packages: createRoute({
    path: routePaths.packages,
    name: 'static-packages',
    component: Packages,
    meta: {
      layout: DefaultLayout,
    },
  }),
  loginModal: createRoute({
    path: routePaths.loginModal,
    name: 'static-home-with-login-modal',
    component: Home,
    meta: {
      layout: ProfileLayout,
    },
  }),
  specialGifts: createRoute({
    path: routePaths.specialGifts,
    name: 'static-special-gifts',
    component: SpecialGifts,
    meta: {
      layout: DefaultLayout,
    },
  }),
  giftsDetail: createRoute({
    path: routePaths.giftDetail,
    name: 'static-gift-detail',
    component: GiftDetail,
    meta: {
      layout: DefaultLayout,
    },
  }),
  specialOverview: createRoute({
    path: routePaths.specialOverview,
    name: 'static-special-overview',
    component: SpecialOverview,
    meta: {
      layout: DefaultLayout,
    },
  }),
  specialDetail: createRoute({
    path: routePaths.specialDetail,
    name: 'static-special-detail',
    component: SpecialDetail,
    meta: {
      layout: DefaultLayout,
    },
  }),
  locationDetail: createRoute({
    path: routePaths.locationDetail,
    name: 'static-location-detail',
    component: LocationDetail,
    meta: {
      layout: DefaultLayout,
    },
  }),
  cart: createRoute({
    path: routePaths.cart,
    name: 'static-cart',
    component: CartPage,
    meta: {
      layout: DefaultLayout,
    },
  }),
  checkout: createRoute({
    path: routePaths.checkout,
    name: 'static-checkout',
    component: CheckoutPage,
    meta: {
      layout: DefaultLayout,
    },
  }),
  orderConfirmation: createRoute({
    path: routePaths.orderConfirmation,
    name: 'static-orderConfirmation',
    component: OrderConfirmation,
    meta: {
      layout: DefaultLayout,
    },
  }),
  instaModal: createRoute({
    path: routePaths.instaModal,
    name: 'static-instaModal',
    component: GiftDetail,
    meta: {
      layout: DefaultLayout,
      demo: {
        component: InstaModal,
        props: {},
      },
    },
  }),
  myAccount: createRoute({
    path: routePaths.myAccount,
    name: 'static-myAccount',
    component: MyAccount,
    meta: {
      layout: DefaultLayout,
    },
  }),
  simpleContent: createRoute({
    path: routePaths.simpleContent,
    name: 'static-simple-content',
    component: SimpleContent,
    meta: {
      layout: DefaultLayout,
    },
  }),
};

export default routes;
