const routePaths = {
  home: '/static-html/home.html',
  locationOverview: '/static-html/location-overview.html',
  about: '/static-html/about.html',
  packages: '/static-html/packages.html',
  valentineSpecial: '/static-html/valentine-special.html',
  loginModal: '/static-html/home-login-modal.html',
  specialGifts: '/static-html/special-gifts.html',
  giftDetail: '/static-html/gift-detail.html',
  specialOverview: '/static-html/special-overview.html',
  specialDetail: '/static-html/special-detail.html',
  locationDetail: '/static-html/location-detail.html',
  cart: '/static-html/cart.html',
  checkout: '/static-html/checkout.html',
  orderConfirmation: '/static-html/order-confirmation.html',
  instaModal: '/static-html/insta-modal.html',
  myAccount: '/static-html/my-account.html',
  sitemap: '/static-html',
  styleGuide: '/static-html/style-guide.html',
  simpleContent: '/static-html/simple-content.html',
};

export default routePaths;
