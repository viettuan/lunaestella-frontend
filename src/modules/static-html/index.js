import AbstractVueModule from '../abstract/index';
import Wrapper from './layouts/Wrapper.vue';
import routes from './routes/index';
import './components/import';
import mockedAxios from './simulator';
import staticStore from './store';

export default class StaticHTMLModule extends AbstractVueModule {
  routes = routes;
  // no need store in static-html
  moduleStore = staticStore;

  moduleName = 'static-html';

  wrapper = Wrapper;

  registerMock() {
    const { apiCaller } = this.context;
    this.removeApiListener = apiCaller.event.on('apiCall', ({ url, options }) => {
      options.axios = mockedAxios;
    });
  }

  onRegister() {
    this.registerMock();
  }

  onEnter = () => {
    document.querySelector('html').classList.add(this.moduleName);
    if (!this.removeApiListener) {
      this.registerMock();
    }
  };

  onExit = () => {
    document.querySelector('html').classList.add(this.moduleName);
    if (this.removeApiListener) {
      this.removeApiListener();
      this.removeApiListener = null;
    }
  };
}
