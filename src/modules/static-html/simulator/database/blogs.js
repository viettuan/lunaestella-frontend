/* eslint-disable */

const blogs = {
  total: 4,
  data: [
    {
      title: 'Love is your life',
      slug: 'love-is-your-life',
      featured: true,
      thumbnail: '',
      double_images: [require('@/assets/uploads/blogs/double-image-1.png'), require('@/assets/uploads/blogs/double-image-2.png')],
      excerpt:
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum.',
    },
    {
      title: 'Lorem ipsum dolor sit amet',
      slug: 'lorem-ipsum-dolor-sit-amet',
      thumbnail: require('@/assets/uploads/blogs/blog-1.png'),
      double_images: [],
      excerpt:
        'Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,no sea takimata sanctus est Lorem ipsum dolor sit amet.',
    },
    {
      title: 'Consetetur sadipscing elitr',
      slug: 'consetetur-sadipscing-elitr',
      thumbnail: require('@/assets/uploads/blogs/blog-2.png'),
      double_images: [],
      excerpt: 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet, consetetur.',
    },
    {
      title: 'At vero eos et accusam',
      slug: 'at-vero-eos-et-accusam',
      thumbnail: require('@/assets/uploads/blogs/blog-3.png'),
      double_images: [],
      excerpt:
        'Et justo duo dolores et ea rebum. Stet clita kasd gubergren,no sea takimata sanctus est Lorem ipsum dolor sit. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
    },
  ],
};

export default blogs;

