const fakeDatabase = {
  config: require('./web-config').default,
  blogs: require('./blogs').default,
};

export default fakeDatabase;

