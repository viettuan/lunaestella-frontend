export default {
  logo: {
    default: require('@/assets/logos/logo-dark.png'),
    light: require('@/assets/logos/logo-light.png'),
    dark: require('@/assets/logos/logo-dark.png'),
  },
  main_menu: [
    {
      id: 12,
      name: 'Special Gifts',
      slug: 'special-gifts',
    },
    {
      id: 13,
      name: 'Unique Location',
      slug: 'unique-location',
    },
    {
      id: 14,
      name: 'Certain Something',
      slug: 'certain-something',
    },
  ],
  footer_menu: [
    {
      id: 15,
      name: 'An Appropriate Surprise',
      slug: 'an-approprite-surprise',
    },
    {
      id: 13,
      name: 'Unique Location',
      slug: 'unique-location',
    },
    {
      id: 14,
      name: 'That Certain Something',
      slug: 'certain-something',
    },
    {
      id: 15,
      name: 'Our nice packages',
      slug: 'our-nice-packages',
    },
  ],
  special_pages: [
    {
      id: 15,
      name: 'Our nice packages',
      slug: 'our-nice-packages',
    },
    {
      id: 22,
      name: "Valentine's day special",
      slug: 'valentine-day-special',
    },
  ],
  privacy_pages: [
    {
      id: 16,
      name: 'Imprint',
      slug: 'imprint',
    },
    {
      id: 17,
      name: 'Disclaimer',
      slug: 'disclaimer',
    },
    {
      id: 18,
      name: 'Terms and Conditions',
      slug: 'term-and-condition',
    },
  ],
  static_pages: [
    {
      id: 19,
      name: 'Blog',
      slug: 'blog',
    },
    {
      id: 20,
      name: 'Contact',
      slug: 'contact',
    },
    {
      id: 21,
      name: 'About',
      slug: 'about',
    },
  ],
  social_links: {
    facebook: 'http://fb.com/demo-page',
    twitter: 'http://twitter.com/demo-page',
    instagram: 'http://instagram.com/demo-page',
  },
  theme_colors: {
    main: '#333f65',
    sub: '#aba093',
    location: '#5f7142',
    packages: '#333f65',
    special: '#8e5885',
  },
  seo: {
    template_title: 'Lunaestella - %s',
    meta: {
      Description: 'Lunaestella sample page description.',
    },
  },
};