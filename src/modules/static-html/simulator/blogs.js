import apiPaths from '@/constants/api/api-paths';
import { delayForATime } from '@/helpers/utils';
import fakeDatabase from './database';

function applyBlogsApiSimulation(mock) {
  // apply simulate get config
  mock.onGet(apiPaths.blogs.base).reply(async (config) => {
    await delayForATime(0);
    return [
      200,
      {
        list: fakeDatabase.blogs.data,
        total: fakeDatabase.blogs.total,
        limit: fakeDatabase.blogs.lenght,
      },
    ];
  });
}

export default applyBlogsApiSimulation;
