/* eslint-disable */
const axios = require('axios');
const deepEqual = require('deep-equal');

function isEqual(a, b) {
  return deepEqual(a, b, { strict: true });
}

// < 0.13.0 will not have default headers set on a custom instance
const rejectWithError = !!axios.create().defaults.headers;

function getUrlParams(str) {
  const query = str.replace(/^([^=]*?\?)|(^\/)/, '');
  let result = null;
  if (query) {
    result = {};
    query.split('&').forEach((part) => {
      const item = part.split('=');
      if (item.length === 2) {
        result[item[0]] = decodeURIComponent(item[1]);
      } else {
        result[item[0]] = true;
      }
    });
  }
  return result;
}

function find(array, predicate) {
  const length = array.length;
  for (let i = 0; i < length; i++) {
    const value = array[i];
    if (predicate(value)) return value;
  }
}

function combineUrls(baseURL, url) {
  if (baseURL) {
    return `${baseURL.replace(/\/+$/, '')}/${url.replace(/^\/+/, '')}`;
  }

  return url;
}

function findHandler(handlers, method, url, body, parameters, headers, baseURL) {
  return find(handlers[method.toLowerCase()], (handler) => {
    if (typeof handler[0] === 'string') {
      return (
        (isUrlMatching(url, handler, parameters) || isUrlMatching(combineUrls(baseURL, url), handler[0])) &&
        isBodyOrParametersMatching(method, body, parameters, handler[1]) &&
        isRequestHeadersMatching(headers, handler[2])
      );
    } else if (handler[0] instanceof RegExp) {
      return (handler[0].test(url) || handler[0].test(combineUrls(baseURL, url))) && isBodyOrParametersMatching(method, body, parameters, handler[1]) && isRequestHeadersMatching(headers, handler[2]);
    }
  });
}

function isUrlMatching(url, handler) {
  const required = handler[0];
  const noSlashUrl = url[0] === '/' ? url.substr(1) : url;
  const noSlashRequired = required[0] === '/' ? required.substr(1) : required;
  if (noSlashUrl === noSlashRequired) {
    return true;
  }
  const queryFromUrl = getUrlParams(url);
  const query = handler[1] && handler[1].query;
  if (queryFromUrl && query) {
    let isEqual = true;
    Object.keys(query).map((key) => {
      if (!(queryFromUrl[key] && query[key].toString() === queryFromUrl[key].toString())) {
        isEqual = false;
      }
    });
    return isEqual;
  }
  return false;
}

function isRequestHeadersMatching(requestHeaders, required) {
  if (required === undefined) return true;
  return isEqual(requestHeaders, required);
}

function isBodyOrParametersMatching(method, body, parameters, required) {
  const allowedParamsMethods = ['delete', 'get', 'head', 'options'];
  if (allowedParamsMethods.indexOf(method.toLowerCase()) >= 0) {
    const params = required ? required.params : undefined;
    return isParametersMatching(parameters, params);
  }
  return isBodyMatching(body, required);
}

function isParametersMatching(parameters, required) {
  if (required === undefined) return true;

  return isEqual(parameters, required);
}

function isBodyMatching(body, requiredBody) {
  if (requiredBody === undefined) {
    return true;
  }
  let parsedBody;
  try {
    parsedBody = JSON.parse(body);
  } catch (e) {}
  return parsedBody ? isEqual(parsedBody, requiredBody) : isEqual(body, requiredBody);
}

function purgeIfReplyOnce(mock, handler) {
  Object.keys(mock.handlers).forEach((key) => {
    const index = mock.handlers[key].indexOf(handler);
    if (index > -1) {
      mock.handlers[key].splice(index, 1);
    }
  });
}

function settle(resolve, reject, response, delay) {
  if (delay > 0) {
    setTimeout(() => {
      settle(resolve, reject, response);
    }, delay);
    return;
  }

  if (response.config && response.config.validateStatus) {
    response.config.validateStatus(response.status) ? resolve(response) : reject(createErrorResponse(`Request failed with status code ${response.status}`, response.config, response));
    return;
  }

  // Support for axios < 0.11
  if (response.status >= 200 && response.status < 300) {
    resolve(response);
  } else {
    reject(response);
  }
}

function createErrorResponse(message, config, response) {
  // Support for axios < 0.13.0
  if (!rejectWithError) return response;

  const error = new Error(message);
  error.config = config;
  error.response = response;
  return error;
}

function isSimpleObject(value) {
  return value !== null && value !== undefined && value.toString() === '[object Object]';
}

export default {
  find,
  findHandler,
  isSimpleObject,
  purgeIfReplyOnce,
  settle,
};
