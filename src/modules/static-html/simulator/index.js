import { create } from 'axios';
import MockAdapter from './axios-mock-adapter/src/index';

import applyCommonAPIsSimulation from './common';
import applyBlogsApiSimulation from './blogs';

const axios = create({});
const mock = new MockAdapter(axios, { delayResponse: 0 });

applyCommonAPIsSimulation(mock);
applyBlogsApiSimulation(mock);

// mock.onAny().passThrough();

export default axios;
