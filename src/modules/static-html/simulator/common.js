import apiPaths from '@/constants/api/api-paths';
import { delayForATime } from '@/helpers/utils';
import fakeDatabase from './database';

function applyCommonAPIsSimulation(mock) {
  // apply simulate get config
  mock.onGet(apiPaths.general.config).reply(async (config) => {
    await delayForATime(0);
    return [
      200,
      fakeDatabase.config,
    ];
  });
}

export default applyCommonAPIsSimulation;
