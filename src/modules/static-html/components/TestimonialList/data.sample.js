export default {
  list: [
    {
      author: 'Peter Miller',
      age: '42',
      content: 'Quote Lorem ipsum dolor sit amet, consetetur nonumy irmod tempor invidunt ut labore et olore magna aliqu a yam erat, sed diam sadetta voluptua.',
      rate: 4,
    },
    {
      author: 'Nora Rodrigez',
      age: '35',
      content: 'At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem justo duo dolores et ea rebum ipsum dolor sit amet.',
      rate: 4,
    },
    {
      author: 'Melanie Cole',
      age: '32',
      content: 'Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim ad assum.',
      rate: 3,
    },
    {
      author: 'Young Bruce',
      age: '50',
      content: 'Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren',
      rate: 5,
    },
  ],
};
