export const ruleInterests = {
  type: 'tags-group',
  name: 'interests',
  label: 'Interests',
  data: {
    choices: [
      {
        title: 'New',
        active: true,
      },
      {
        title: 'BestSeller',
        active: false,
      },
      {
        title: 'Discount',
        active: false,
      },
      {
        title: 'ValentinesDay',
        active: false,
      },
      {
        title: 'birthday',
        active: true,
      },
      {
        title: 'at home',
        active: false,
      },
      {
        title: 'christmas',
        active: false,
      },
    ],
  },
};

export default {
  filterType: 'gifts',
  calendar: null,
  rules: [
    {
      type: 'tags-group',
      name: 'category',
      label: 'Category',
      data: {
        choices: [
          {
            title: 'Man',
            active: true,
          },
          {
            title: 'Woman',
            active: true,
          },
          {
            title: 'Giftbox',
            active: false,
          },
          {
            title: 'Clothes',
            active: false,
          },
          {
            title: 'Sense & Soul',
            active: false,
          },
        ],
      },
    },
    {
      type: 'tags-group',
      name: 'subcategory',
      label: 'Subcategory',
      data: {
        choices: [
          {
            title: 'Subcat 1',
            active: true,
          },
          {
            title: 'Subcat 2',
            active: false,
          },
          {
            title: 'Subcat 3',
            active: false,
          },
          {
            title: 'Subcat 4',
            active: false,
          },
          {
            title: 'Subcat 5',
            active: false,
          },
        ],
      },
    },
    ruleInterests,
    {
      type: 'price-slider',
      name: 'price',
      label: 'Price',
      data: {
        unit: null,
      },
    },
  ],
};
