import $icons from '../../../../../common/icons';

export const ruleCategory = {
  type: 'tags-group',
  name: 'category',
  label: 'Category',
  data: {
    choices: [
      {
        title: 'Wellness',
        icon: $icons.bath,
        active: false,
      },
      {
        title: 'Countryside',
        icon: $icons.nature,
        active: true,
      },
      {
        title: 'Luxury',
        icon: $icons.luxury,
        active: true,
      },
      {
        title: 'Food',
        icon: $icons.kitchen,
        active: false,
      },
    ],
  },
};

export const ruleRegion = {
  type: 'tags-group',
  name: 'region',
  label: 'Region',
  data: {
    choices: [
      {
        title: 'Italy',
        active: false,
      },
      {
        title: 'Austria',
        active: false,
      },
      {
        title: 'Switzerland',
        active: true,
      },
      {
        title: 'France',
        active: false,
      },
      {
        title: 'Germany',
        active: false,
      },
      {
        title: 'Spain',
        active: false,
      },
      {
        title: 'Portugal',
        active: false,
      },
      {
        title: 'Unitied Kingdom',
        active: false,
      },
    ],
  },
};

export default {
  filterType: 'packages',
  calendar: {
    from: true,
    to: true,
    nights: true,
  },
  rules: [
    {
      type: 'price-slider',
      name: 'price',
      label: 'Price',
      data: {
        unit: 'per night for two persons',
      },
    },
    ruleCategory,
    ruleRegion,
    {
      type: 'tags-group',
      name: 'interests',
      label: 'Interests',
      data: {
        choices: [
          {
            title: 'Dining',
            active: false,
          },
          {
            title: 'Weekend',
            active: false,
          },
          {
            title: 'Action',
            active: false,
          },
          {
            title: 'Nature',
            active: true,
          },
        ],
      },
    },
  ],
};
