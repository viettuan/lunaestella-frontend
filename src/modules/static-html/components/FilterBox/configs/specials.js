import $icons from '../../../../../common/icons';
import { ruleCategory, ruleRegion } from './packages';
import { ruleInterests } from './gifts';

const customRuleCategory = {
  ...ruleCategory,
  data: {
    choices: [
      ...ruleCategory.data.choices,
      {
        title: 'City',
        icon: $icons.city,
        active: false,
      },
      {
        title: 'Transfer',
        icon: $icons.car,
        active: false,
      },
    ],
  },
};

export default {
  filterType: 'gifts',
  calendar: {
    from: true,
    to: true,
    nights: false,
    fromLabel: 'Start date',
    toLabel: 'End date',
  },
  rules: [
    {
      type: 'checkbox-group',
      name: 'combine',
      label: 'Combine',
      data: {
        isMultiple: false,
        choices: [
          {
            title: '<b>Can be combined</b> with the selected hotel',
            value: 1,
            checked: false,
          },
          {
            title: "<b>Can't be combined</b> with the selected hotel",
            value: 0,
            checked: true,
          },
        ],
      },
    },
    customRuleCategory,
    ruleRegion,
    ruleInterests,
    {
      type: 'price-slider',
      name: 'price',
      label: 'Price',
      data: {
        unit: null,
      },
    },
  ],
};
