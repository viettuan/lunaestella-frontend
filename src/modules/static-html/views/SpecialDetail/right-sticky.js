import { onResize } from '@/helpers/dom/resize';
import { HEADER_HEIGHT_DESKTOP, HEADER_HEIGHT_TABLET_MOBILE } from '@/common/display';
import Sticky from '@/js/packages/sticky-js';
import { safeRequestAnimationFrame } from '../../../../helpers/dom/request-animation-frame';

export const rightStickyMixin = {
  mounted() {
    let lastDevice = this.$viewport.device;
    this.initSticky();
    this.listenResize = onResize(() => {
      if (lastDevice !== this.$viewport.device) {
        lastDevice = this.$viewport.device;
        this.$forceUpdate();
      }
      safeRequestAnimationFrame(() => {
        this.initSticky();
      });
    });
  },
  beforeDestroy() {
    this.destroySticky();
    if (this.listenResize) {
      this.listenResize.remove();
    }
  },
  computed: {
    headerHeight() {
      return this.$viewport.isDesktop ? HEADER_HEIGHT_DESKTOP : HEADER_HEIGHT_TABLET_MOBILE;
    },
  },
  methods: {
    initRightPad() {
      const rightPad = this.$el.querySelector('.right-pad');
      if (rightPad) {
        const container = this.$el.querySelector('.sticky-wrapper');
        if (!this.$viewport.isMobile) {
          const width = window.innerWidth - container.getBoundingClientRect().right;
          rightPad.style.width = `${width}px`;
        } else {
          rightPad.style.width = 'auto';
        }
      }
    },
    initSticky() {
      this.destroySticky();
      this.resetStickyStyle();
      const headerHeight = this.$viewport.isDesktop ? HEADER_HEIGHT_DESKTOP : HEADER_HEIGHT_TABLET_MOBILE;
      if (!this.$viewport.isMobile) {
        this.stickyAction = new Sticky('.sticky-action', {
          marginTop: headerHeight,
          stickyClass: 'is-sticky',
          stickyContainer: '.col-aside',
          stickyFor: 767,
        });
        this.initRightPad();
      } else {
        this.stickyPrice = new Sticky('.sticky-action__footer', {
          marginTop: window.innerHeight - 115,
          stickyClass: 'no-sticky',
        });
      }
    },
    destroySticky() {
      if (this.stickyAction) {
        this.stickyAction.destroy();
      }
      if (this.stickyPrice) {
        this.stickyPrice.destroy();
      }
    },
    resetStickyStyle() {
      const stickyAction = this.$el.querySelector('.sticky-action');
      const stickyPrice = this.$el.querySelector('.sticky-action__footer');
      stickyAction.style = '';
      stickyPrice.style = '';
    },
  },
};
