import { objectMap } from '@/helpers/utils';
import { onScroll } from '../../../../helpers/dom/scroll';
import { getHeaderHeight } from '../../../../common/display';

export default function initScrollSpy(selector, { hrefContainer = '', offset } = {}) {
  const section = document.querySelectorAll(selector);
  const sections = {};
  const hrefList = document.querySelector(hrefContainer);

  Array.prototype.forEach.call(section, (e) => {
    sections[e.id] = e.offsetTop;
  });

  const scrollListen = onScroll(() => {
    const scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
    objectMap(sections, (e, i) => {
      const headerHeight = getHeaderHeight();
      if (e <= scrollPosition + headerHeight + offset) {
        const active = hrefList.querySelector('.active');
        const href = hrefList.querySelector(`[data-target*=${i}]`);
        if (active) {
          active.classList.remove('active');
        }
        if (href) {
          href.classList.add('active');
        }
      }
    });
  });
  return {
    remove: () => {
      scrollListen.remove();
    },
  };
}
