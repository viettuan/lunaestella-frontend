import { createRoute } from '@/helpers/vue/route-helper';
// layouts
import BlankLayout from '@/modules/static-html/layouts/BlankLayout';
// pages
import Sitemap from '@/modules/static-html/views/Sitemap';

import routePaths from './route-paths';

const routes = {
  index: createRoute({
    path: routePaths.index,
    name: 'main-app-index',
    component: Sitemap,
    meta: {
      layout: BlankLayout,
    },
  }),
};

export default routes;
