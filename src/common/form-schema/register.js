export default function () {
  return yup => ({
    firstName: yup
      .string()
      .required()
      .label('First Name'),
    surname: yup.string().required(),
    username: yup
      .string()
      .matches(/\w{0,100}/, 'Username is invalid')
      .required(),
    email: yup
      .string()
      .email()
      .required(),
    password: yup
      .string()
      .min(6)
      .required(),
  });
}
