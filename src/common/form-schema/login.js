export default function () {
  return yup => ({
    username: yup.string().required(),
    password: yup.string().required(),
  });
}
