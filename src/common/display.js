import { onResize } from '../helpers/dom/resize';

const { isClient, isServer } = global;

export const WIDTH_TABLET = 1199;
export const WIDTH_MOBILE = 767;
export const HEADER_HEIGHT_DESKTOP = 92;
export const HEADER_HEIGHT_TABLET_MOBILE = 72;

export function isMobileAndTablet() {
  return isClient && window.innerWidth < WIDTH_TABLET;
}

export function isMobile() {
  return isClient && window.innerWidth < WIDTH_MOBILE;
}

export function isTablet() {
  return isClient && window.innerWidth <= WIDTH_TABLET && window.innerWidth > WIDTH_MOBILE;
}

export function isDesktop() {
  return isServer || window.innerWidth > WIDTH_TABLET;
}

function width() {
  return isServer || window.innerWidth;
}

function device() {
  // eslint-disable-next-line no-nested-ternary
  return isMobile() ? 'mobile' : isTablet() ? 'tablet' : 'desktop';
}

export function checkViewport() {
  const res = {};
  function check() {
    const lastDevice = res.device;
    const functions = {
      isMobileAndTablet,
      isMobile,
      isDesktop,
      isTablet,
      width,
      device,
    };
    Object.entries(functions).map(([key, value]) => (res[key] = value()));
    if (isClient && lastDevice && lastDevice !== res.device) {
      $(document).trigger('device:changed', lastDevice, res.device);
    }
  }
  if (isClient) onResize(check);
  check();
  return res;
}

export function getHeaderHeight() {
  const defaultHeight = isDesktop() ? HEADER_HEIGHT_DESKTOP : HEADER_HEIGHT_TABLET_MOBILE;
  const stickyMenu = document.querySelector('.section-menus .is-sticky');
  return defaultHeight + (stickyMenu ? stickyMenu.clientHeight : 0);
}

export const onDeviceChangedMixin = {
  mounted() {
    if (isClient) {
      $(document).on('device:changed', this.handler);
    }
  },
  methods: {
    handler(event, last, current) {
      this.onDeviceChanged(last, current);
    },
    onDeviceChanged(last, current) {
      console.log(`- Please implement method: onDeviceChanged in ${this.$options.name}`);
    },
  },
  beforeDestroy() {
    if (isClient) {
      $(document).off('device:changed', this.handler);
    }
  },
};
