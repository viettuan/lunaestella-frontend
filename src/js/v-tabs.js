import Vue from 'vue';
import { triggerWindowResized } from '../helpers/utils/index';

function createTabMenu(el) {
  const { $allTabs } = el;
  const $tabMenu = $('<div class="tabs__menu"><ul></ul></div>');
  const $ul = $tabMenu.find('ul');
  const activeId = $allTabs.index($(el).find('.tab.active'));
  $allTabs.find('.tab__title').each((i, e) => {
    const $li = $(
      `<li><a href="javascript:void(0);" name="${$(e)
        .parent()
        .attr('name') || i}">${e.innerHTML}</a></li>`,
    );
    if (i === activeId) {
      $li.find('a').addClass('active');
    }
    $ul.append($li);
  });
  $(el).prepend($tabMenu);
}

Vue.directive('tabs', {
  inserted(el) {
    const $this = $(el);

    el.$allTabs = $this.find('.tab');
    createTabMenu(el);
    el.$tabsContainer = $this.find('.tabs');
    el.$menuList = $this.find('.tabs__menu li');
    el.$menuHrefs = $this.find('.tabs__menu li > a');

    el.$menuHrefs.click((event) => {
      const $a = $(event.target);
      const id = el.$menuList.index($a.parent());
      const $tab = el.$allTabs.eq(id);
      el.$menuHrefs.removeClass('active');
      el.$allTabs.removeClass('active');
      $a.addClass('active');
      $tab.addClass('active');
      $this.trigger('change', $tab.attr('name'), id);
    });

    $this.on('click touchend', '.tab .tab__title', (event) => {
      event.stopPropagation();
      event.preventDefault();
      const $this = $(event.target);
      $this.parents('.tab').toggleClass('expand');
      triggerWindowResized();
    });

    // functions
    $this.on('set', (event, tabname) => {
      const target = el.$menuHrefs.toArray().find(e => e.name === tabname);
      if (target) {
        target.click();
      }
    });
  },
});
