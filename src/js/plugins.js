import pageLoader from '@/helpers/user-interface/page-loader';
import dev from '@/helpers/dev';
import './vue-click-outside';

global.isServer = typeof window === 'undefined';
global.isClient = typeof window !== 'undefined';
global.pageLoader = pageLoader;
global.dev = dev;

// expose global variable
global.jQuery = $;

/* eslint-disable*/
String.prototype.toInt = function toInt() {
  return parseInt(this);
};

String.prototype.toCapitalize = function toCapitalize() {
  return this.replace(/\b\w/g, l => l.toUpperCase());
};

require('./theme-features');