import utils, { triggerWindowResized } from '../helpers/utils/index';
import { getHeaderHeight } from '../common/display';

if (global.isClient) {
  global.nanobar = new (require('nanobar'))();

  $(document).on('click', '.form-check label', (event) => {
    event.preventDefault();
    event.stopImmediatePropagation();
    $(event.currentTarget)
      .siblings('input')
      .click();
  });

  $(document).on('click touchend', '[toggle-class]', (event) => {
    event.preventDefault();
    event.stopImmediatePropagation();
    const $current = $(event.currentTarget);
    const className = $current.attr('toggle-class');
    const toggleTarget = $current.attr('toggle-target');
    const toggleClosest = $current.attr('toggle-closest');
    let $target = $current;
    if (toggleClosest) {
      $target = $current.parents(toggleClosest);
    } else if (toggleTarget) {
      $target = $(toggleTarget);
    }
    $target.toggleClass(className);
  });

  // collapse group
  $(document).on('click', '.collapse-group .group__title', (e) => {
    e.stopPropagation();
    const { currentTarget } = e;
    const triggerResize = $(currentTarget).data('resize');
    const groupContainer = $(currentTarget).closest('.collapse-group-container');
    const isTryToOpen = !$(currentTarget).hasClass('show');
    if (groupContainer && isTryToOpen) {
      const otherGroupTitles = $(groupContainer)
        .find('.group__title.show')
        .not(currentTarget);
      otherGroupTitles.removeClass('show');
    }
    if (triggerResize !== undefined) {
      triggerWindowResized();
    }
    $(currentTarget).toggleClass('show');
  });

  // scroll to
  $(document).on('click', '.scroll-to', (event) => {
    event.preventDefault();
    event.stopPropagation();
    const target = $(event.target).data('target');
    const offset = $(event.target).data('offset');
    utils.scrollToElement(target, offset || getHeaderHeight());
  });

  $('html').addClass('client-mode');

  // import
  require('./v-tabs');
}
