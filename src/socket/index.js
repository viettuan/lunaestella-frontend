import io from 'socket.io-client';

class Socket {
  server = null;

  socket = null;

  responseHandler = null;

  constructor({ server, responseHandler }) {
    this.server = server;
    this.responseHandler = responseHandler;
  }

  connect() {
    this.socket = io(this.server);
  }

  disconnect() {

  }

  refresh() {
  }

  async emit(eventName, options) {
    try {
      const resp = await this.socket.emit(eventName, options.data);
      this.responseHandler.handleSuccess(resp);
      return resp;
    } catch (err) {
      this.responseHandler.handleError(err);
      return err;
    }
  }

  listen(eventName, handler) {
    return this.socket.on(eventName, handler);
  }
}

export default Socket;
