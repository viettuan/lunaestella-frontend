export default {
  locale: 'en',
  user: {
    login: 'login',
    pleaseLogin: 'Please login',
    logout: 'log out',
    register: 'register',
    username: 'username',
    password: 'password',
    email: 'email',
  },
  action: {
    back: 'back',
    close: 'close',
    search: 'search',
  },
  menu: {
    contact: 'contact',
    about: 'about',
    blog: 'blog',
  },
  cart: {
    yourCart: 'your cart',
    addToCart: 'add to cart',
  },
  payment: {
    number: 'Number',
    owner: 'Owner',
    cvcode: 'CV-code',
    month: 'Month',
    year: 'Year',
  },
};
