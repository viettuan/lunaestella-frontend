export default {
  locale: 'fr',
  user: {
    login: 'log[fr]',
    pleaseLogin: 'Please login[fr]',
    logout: 'log out[fr]',
    register: 'register[fr]',
    username: 'username[fr]',
    password: 'password[fr]',
    email: 'email[fr]',
  },
  action: {
    back: 'back[fr]',
    close: 'close[fr]',
    search: 'search[fr]',
  },
  menu: {
    contact: 'contact[fr]',
    about: 'about[fr]',
    blog: 'blog[fr]',
  },
  cart: {
    yourCart: 'your cart[fr]',
    addToCart: 'add to cart[fr]',
  },
};
