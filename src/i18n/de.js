export default {
  locale: 'de',
  user: {
    login: 'log[de]',
    pleaseLogin: 'Please login[de]',
    logout: 'log out[de]',
    register: 'register[de]',
    username: 'username[de]',
    password: 'password[de]',
    email: 'email[de]',
  },
  action: {
    back: 'back[de]',
    close: 'close[de]',
    search: 'search[de]',
  },
  menu: {
    contact: 'contact[de]',
    about: 'about[de]',
    blog: 'blog[de]',
  },
  cart: {
    yourCart: 'your cart[de]',
    addToCart: 'add to cart[de]',
  },
};
